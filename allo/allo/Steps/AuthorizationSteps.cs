﻿using allo.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace allo.Steps
{
    [Binding]
    public class AuthorizationSteps
    {
        public IWebDriver driver;
        public MainPage mainPage;
        public LoginForm loginForm;

        //[BeforeScenario]
        //public void CreateDriver()
        //{
        //    driver = new ChromeDriver(@"C:\driver");
        //    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(5);

        //}
        //[Given(@"Allo website is open")]
        //public void GivenAlloWebsiteIsOpen()
        //{
        //    driver.Navigate().GoToUrl("https://allo.ua");
        //    driver.Manage().Window.Maximize();
        //    mainPage = new MainPage(driver);
        //    loginForm = new LoginForm(driver);

        //}
        [When(@"User clicks on login button")]
        public void WhenUserClicksOnLoginButton()
        {
            mainPage.ClickLoginButton();
        }
        
        [When(@"User enter on field e-mail ""(.*)""")]
        public void WhenUserEnterOnFieldE_Mail(string p0)
        {
            loginForm.EnterFieldEmail();
        }
        
        [When(@"User enter on field password ""(.*)""")]
        public void WhenUserEnterOnFieldPassword(string p0)
        {
            loginForm.EnterFieldPassword();
        }
        
        [When(@"User clicks on button Enter")]
        public void WhenUserClicksOnButtonEnter()
        {
            loginForm.ClickLoginButton();
        }
        
        [Then(@"User authorized")]
        public void ThenUserAuthorized()
        {
            string userAuthorizedLabel= mainPage.GetTextUserAuthorizedLabel();
            Assert.AreEqual("Любовь", userAuthorizedLabel);
        }
    }
}
