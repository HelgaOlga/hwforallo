﻿using allo.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace allo.Steps
{
    [Binding]
    public class FeedbackFormSteps
    {
        public IWebDriver driver;
        public MainPage mainPage;
        public FeedbackFormPage feedbackFormPage;

        //[BeforeScenario]
        //public void CreateDriver()
        //{
        //    driver = new ChromeDriver(@"C:\driver");
        //    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(5);

        //}
        //[Given(@"Allo website is open")]
        //public void GivenAlloWebsiteIsOpen()
        //{
        //    driver.Navigate().GoToUrl("https://allo.ua");
        //    driver.Manage().Window.Maximize();
        //    mainPage = new MainPage(driver);
        //    feedbackFormPage = new FeedbackFormPage(driver);


        //}
        [When(@"User clicks on Contacts button")]
        public void WhenUserClicksOnContactsButton()
        {
            mainPage.ClickContactsButton();
        }
        
        [When(@"User enter on field Name ""(.*)""")]
        public void WhenUserEnterOnFieldName(string p0)
        {
            feedbackFormPage.EnterFieldName();

        }
        [When(@"User enter on field e-mail ""(.*)""")]
        public void WhenUserEnterOnFieldE_Mail(string p0)
        {
            feedbackFormPage.EnterFieldEmail();
        }

        [When(@"User enter on field message ""(.*)""")]
        public void WhenUserEnterOnFieldMessage(string p0)
        {
            feedbackFormPage.EnterFieldMessage();
        }
        
        [Then(@"User Contacts Sent")]
        public void ThenUserContactsSent()
        {
            string ContactsSent = mainPage.GetTextFeedbackFormPageLabel();
            Assert.AreEqual("Ваше повідомлення надіслано в обробку.", ContactsSent);
        }
    }
}
