﻿using allo.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace allo.Steps
{
    [Binding]
    public class FishkaSteps
    {
        public IWebDriver driver;
        public MainPage mainPage;
        public FishkaPage fishkaPage;

        //[BeforeScenario]
        //public void CreateDriver()
        //{
        //    driver = new ChromeDriver(@"C:\driver");
        //    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(5);

        //}
        //[Given(@"Allo website is open")]
        //public void GivenAlloWebsiteIsOpen()
        //{
        //    driver.Navigate().GoToUrl("https://allo.ua");
        //    driver.Manage().Window.Maximize();
        //    mainPage = new MainPage(driver);
        //    fishkaPage = new FishkaPage(driver);

        //}
        [When(@"User clicks on Fishka  button")]
        public void WhenUserClicksOnFishkaButton()
        {
            mainPage.ClickFishkaButton();
        }
        
        [Then(@"Page Fishka  open")]
        public void ThenPageFishkaOpen()
        {
            string titleFishka = fishkaPage.GetFishkaText();
            Assert.AreEqual("АЛЛО разом з FISHKA!", titleFishka);
        }
    }
}
