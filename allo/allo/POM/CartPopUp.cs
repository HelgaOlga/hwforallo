﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace allo.POM
{
    public class CartPopUp
    {
        private IWebDriver _driver;
        public By TitleCart = By.XPath("/html/body/div[4]/div/div/div[2]/span");
        public By ClickertHereText = By.XPath("/html/body/div[4]/div/div/div[3]/div/div[1]/div/p[2]/a");
        public By CartIsEmptyText = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/p[1]");
        public By ExitButton = By.XPath("/html/body/div[4]/div/div/div[1]/svg/use//svg/path");

       

        public CartPopUp(IWebDriver driver)
        {
            this._driver = driver;

        }

       



        public IWebElement FindElementCartMainLabel()
        {
            return _driver.FindElement(CartIsEmptyText);

        }
        public string GetTextCart()
        {
            return FindElementCartMainLabel().Text;
        }
    }
}
