﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace allo.POM
{
   public class SearchPage
    {
        private IWebDriver _driver;
        public By SearchMainLabel = By.XPath("/html/body/div[1]/div/div/div[2]/ul/li[2]/a");

        public SearchPage(IWebDriver driver)
        {
            this._driver = driver;

        }

        public IWebElement FindElementSearchLabel()
        {
            return _driver.FindElement(SearchMainLabel);

        }
        public string GetTextSearchLabel()
        {
            return FindElementSearchLabel().Text;
        }
    }
}
