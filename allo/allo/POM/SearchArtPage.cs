﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace allo.POM
{
   public class SearchArtPage
    {
         
        private IWebDriver _driver;
        public By SearchMainArtLabel = By.XPath("/html/body/div[1]/div/div/div[2]/ul/li[2]/span/b");

        public SearchArtPage(IWebDriver driver)
        {
            this._driver = driver;

        }

        public IWebElement FindElementSearchArtLabel()
        {
            return _driver.FindElement(SearchMainArtLabel);

        }
        public string GetTextSearchArtLabel()
        {
            return FindElementSearchArtLabel().Text;
        }

    }
}
