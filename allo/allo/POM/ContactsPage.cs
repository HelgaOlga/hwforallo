﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace allo.POM
{
   public class ContactsPage
    {
        
        private IWebDriver _driver;
        public By TitleContacts = By.XPath("/html/body/div[1]/div/div/div[2]/ul/li[2]/span");

        public ContactsPage(IWebDriver driver)
        {
            this._driver = driver;

        }

        public IWebElement FindElementContactsMainLabel()
        {
            return _driver.FindElement(TitleContacts);

        }
        public string GetTextContacts()
        {
            return FindElementContactsMainLabel().Text;
        }
    }
}
