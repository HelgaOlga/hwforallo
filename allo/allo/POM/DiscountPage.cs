﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace allo.POM
{
    public class DiscountPage
    {
        private IWebDriver _driver;
        public By TitleDiscount = By.XPath("/html/body/div[1]/header/div[3]/div/h1");

        public DiscountPage (IWebDriver driver)
        {
            this._driver = driver;

        }

        public IWebElement FindElementDiscountMainLabel()
        {
            return _driver.FindElement(TitleDiscount);

        }
        public string GetDiscountText()
        {
            return FindElementDiscountMainLabel().Text;
        }

    }
}
