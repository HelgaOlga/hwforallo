﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace allo.POM
{
   public class AlloObminPage
    {
        private IWebDriver _driver;
        public By TitleAlloObmin= By.XPath("/html/body/div[3]/div[2]/div[1]/ul/li[2]/span");
       
        public AlloObminPage(IWebDriver driver)
        {
            this._driver = driver;

        }

        public IWebElement FindElementAlloObminMainLabel()
        {
            return _driver.FindElement(TitleAlloObmin);

        }
        public string GetTextAlloObmin()
        {
            return FindElementAlloObminMainLabel().Text;
        }


    }
}
