﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace allo.POM
{
    public class LoginForm
    {
        private IWebDriver _driver;

        public By EmailField = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[1]/div[1]/input");
        public By PasswordField = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[2]/div[1]/input");
        public By LoginButton = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/button");

        public LoginForm(IWebDriver driver)
        {
            this._driver = driver;

        }
        public LoginForm EnterFieldEmail()
        {
            _driver.FindElement(EmailField).SendKeys("test@gmail.com");
            return this;
        }
        public LoginForm EnterFieldPassword()
        {
            _driver.FindElement(PasswordField).SendKeys("p@$$w0rd");
            return this;
        }
        public MainPage ClickLoginButton()
        {
            _driver.FindElement(LoginButton).Click();
            return new MainPage(_driver);

        }

    }
}
