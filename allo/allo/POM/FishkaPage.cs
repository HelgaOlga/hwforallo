﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace allo.POM
{
   public class FishkaPage
    {
        private IWebDriver _driver;
        public By TitleFishka = By.XPath("/html/body/div[3]/div[2]/div[1]/ul/li[2]/span");

        public FishkaPage(IWebDriver driver)
        {
            this._driver = driver;

        }

        public IWebElement FindElementFishkaMainLabel()
        {
            return _driver.FindElement(TitleFishka);

        }
        public string GetFishkaText()
        {
            return FindElementFishkaMainLabel().Text;
        }


    }
}
