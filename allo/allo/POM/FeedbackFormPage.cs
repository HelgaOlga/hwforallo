﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace allo.POM
{
    public class FeedbackFormPage
    {
        private IWebDriver _driver;

        public By EmailField = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/div[2]/div/div[2]/div/input");
        public By NameField = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/div[1]/div/div/input");
        public By MessageField = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/div[4]/div/div/textarea");
        public By LoginButton = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/button");

        public FeedbackFormPage(IWebDriver driver)
        {
            this._driver = driver;

        }
        public FeedbackFormPage EnterFieldName()
        {
            _driver.FindElement(NameField).SendKeys("Helga");
            return this;
        }
        public FeedbackFormPage EnterFieldEmail()
        {
            _driver.FindElement(EmailField).SendKeys("test@gmail.com");
            return this;
        }
        public FeedbackFormPage EnterFieldMessage()
        {
            _driver.FindElement(MessageField).SendKeys("Say Hi!");
            return this;
        }
        
        public MainPage ClickLoginButton()
        {
            _driver.FindElement(LoginButton).Click();
            return new MainPage(_driver);

        }
    }
}
