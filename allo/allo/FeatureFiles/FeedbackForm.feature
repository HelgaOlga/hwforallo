﻿Feature: FeedbackForm
	As a user
	I want to be able to leave my contacts
	In order to so that I can be contacted

Scenario: Leave contacts for feedback
	Given Allo website is open
	When User clicks on Contacts button
	When User enter on field Name "Helga"
	When User enter on field e-mail "test@gmail.com"
	When User enter on field message "Say Hi!"
	When User clicks on button Enter
    Then User Contacts Sent